struct IOSCollection {
	var value = 0
}

class Ref<T> {
	var value: T
	init (value: T) {
		self.value = value
	}
} 
struct Conteuner<T> {
	var ref: Ref<T>
	init (value: T) {
		self.ref = Ref(value: value)
	} 
	var value: T {
		get { ref.value }
		set {
			guard(isKnownUniquelyReferenced(&ref)) else {
				ref = Ref(value: newValue) 
				return
			}
			ref. value = newValue
		}
	}
}


protocol Hotel {
	init (roomCount: Int)
}
class HotelAlfa: Hotel {
	var roomCount: Int
	required init (roomCount: Int) { 
		self.roomCount = roomCount
	}
}

protocol GameDice {
	var numberDice: String {
		get
	}
}

extension Int: GameDice {
	var numberDice: String {
	return "Выпало \(self) на кубике"
	}
}
let diceCoub = 4 
print(diceCoub.numberDice)

@objc protocol Person {
	var name: String {get set}
	@objc optional var patronymic: String {get set}
	func printName()
}

class ChildPerson: Person {
	var name: String

	func printName() {
		print(name)
	}

	init(name: String) {
		self.name = name
	}
	
}

enum Platform: String {
	case Ios = "IOS"
	case Android = "Android"
	case Web = "Web"
}

protocol WriteProtocol {
	var time: Int {get set}
	var codeLines: Int {get set}
	func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol StopProtocol {
	func stopCoding()
}
class Company: WriteProtocol, StopProtocol {
	var time: Int
	var codeLines: Int
	var numberOfSpecialist: Int
	var platform: Platform
	

	init ( time: Int, codeLines: Int, numberOfSpecialist: Int, platform: Platform) {
		self.time = time
		self.codeLines = codeLines
		self.numberOfSpecialist = numberOfSpecialist
		self.platform = platform
	}

	func writeCode(platform: Platform, numberOfSpecialist: Int) {
		print("Разработка началась, пишем код под \(platform.rawValue), кол-во программистов: \(numberOfSpecialist)")
	}

	func stopCoding() {
		print("Работа закончена, сдаю в тестирование.")
	}

}